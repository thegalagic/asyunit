<!--
SPDX-FileCopyrightText: 2020-3 Galagic Limited, et al. <https://galagic.com>

SPDX-License-Identifier: CC-BY-SA-4.0

Asyunit is a test runner for the Asymptote vector graphics language.

For full copyright information see the AUTHORS file at the top-level
directory of this distribution or at
[AUTHORS](https://gitlab.com/thegalagic/asyunit/AUTHORS.md)

This work is licensed under the Creative Commons Attribution 4.0 International
License. You should have received a copy of the license along with this work.
If not, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to
Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
-->

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [3.0.0] - 2023-09-21

### Added

* You can now install `asyunit` with [bin](https://github.com/marcosnils/bin).

### Changed

* Changed repository name to `asyunit`, updated license headers and docs etc.
* Update galagos template to v1.0.2

### Fixed

* Expectations should not fail just because the version of Asymptote has
  changed. Two different versions may still produce the same output.

## [2.0.0] - 2022-08-09

### Added

* Noted dependencies in `CONTRIBUTING`.
* For .eps expectations create PNGs of the actual and expected results for easy
  comparison (alongside the existing diff PNG we create). This is because since
  asy 2.69+ the .eps files created are not easily viewable in desktop apps due
  to addition of custom postscript cmds (more on this below).

### Changed

* New command line interface. Now the only argument is the test fixture,
  everything else is set by flags:
  * `-g dir` to indicate these are graphical tests and set the artifact dir.
    Shipout will be called.
  * `-k pattern` to select which tests to run.
  * For full info run `asyunit -h`.
* Tests units are now fully isolated from each other. We achieve this by
  supplying each test unit in its own file to asy which then runs them each in
  their own environment.
* Move closer to TAP spec by writing test failure information as 'diagnostic lines'
  i.e. "# diagnostic" after the "ok/not ok" line.
* Diagnostic lines now appear BEFORE the test line they relate to (the 'ok/not
  ok' line) and in real-time. Previously we waited for asy to completely finish
  before showing output. So we can see feedback from a failing test as soon
  as its available.
* Don't output diff of expectations against actual - it's usually large in
  real world situations and duplicates our own test diagnostic message.
* Update test expectations for circle as since asy v2.69+ new transparency
  commands are present.
* Updated test dependencies to latest.

### Fixed

* When a fixture is not found give return code of 1 and a better message
* We were not checking that every expectation file provided as input for a test
  had a corresponding output. This meant for example if a test stopped producing
  PDFs it would not fail. Now we ensure that if we expect a certain file
  we fail if it is not produced. '.license' files are ignored.
* The ImageMagick compare was broken since asymptote v2.69+. Asymptote's
  intermediate postscript files (.eps) have GhostScript custom transparency
  commands that require a GhostScript flag to enable. This causes an issue when
  we run the ImageMagick compare as it uses GhostScript under the hood. So now
  we configure IM before we run the compare. More info:
  [Ghostscript and the PostScript Language](https://www.ghostscript.com/doc/9.52/Language.htm#Transparency)
* Also regarding ImageMagick compare we fix a warning message about colour
  profiles by forcing it to use full colour instead of downgrading to grayscale
  when producing diff PNG.
* We were creating a new currentpicture between test runs. If any global code
  was taking a copy/reference to currentpicture this would no longer refer to
  the real currentpicture after it was replaced at shipout. Now we simply
  `erase()` currentpicture after a run.

## [1.0.0] - 2020-10-19

### Added

* First version

[1.0.0]: https://gitlab.com/thegalagic/asyunit/releases/tag/v1.0.0
[2.0.0]: https://gitlab.com/thegalagic/asyunit/releases/tag/v2.0.0
