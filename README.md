<!--
SPDX-FileCopyrightText: 2020-3 Galagic Limited, et al. <https://galagic.com>

SPDX-License-Identifier: CC-BY-SA-4.0

Asyunit is a test runner for the Asymptote vector graphics language.

For full copyright information see the AUTHORS file at the top-level
directory of this distribution or at
[AUTHORS](https://gitlab.com/thegalagic/asyunit/AUTHORS.md)

This work is licensed under the Creative Commons Attribution 4.0 International
License. You should have received a copy of the license along with this work.
If not, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to
Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
-->

# asyunit

[![CII Best Practices](https://bestpractices.coreinfrastructure.org/projects/4351/badge)](https://bestpractices.coreinfrastructure.org/projects/4351)

A test runner for the [Asymptote](https://asymptote.sourceforge.io/) vector
graphics language.

The official repository is [https://gitlab.com/thegalagic/asyunit/](https://gitlab.com/thegalagic/asyunit/)

## Table of Contents

<!-- vim-markdown-toc GitLab -->

* [Background](#background)
* [Install](#install)
  * [Via bin](#via-bin)
  * [Via git clone](#via-git-clone)
* [Usage](#usage)
* [Writing Tests](#writing-tests)
* [To Do](#to-do)
* [Contributing](#contributing)
* [License](#license)

<!-- vim-markdown-toc -->

## Background

As with any language when writing large or long-lived code it can be important
to have a way to test your code and ensure the final output is what you expect.
Setting expectations can also insure that when you upgrade your system you are
made aware of any changes in how your pictures will be rendered.

## Install

You'll need a recent version of BASH and optionally ImageMagick 6 (for graphical
tests). There are two ways to install:

### Via bin

**RECOMMENDED** Install with [bin](https://github.com/marcosnils/bin):

```bash
bin install https://gitlab.com/thegalagic/asyunit/
```

bin will allow you to manage the install and update in future.

### Via git clone

```bash
git clone https://gitlab.com/thegalagic/asyunit/
```

Then copy asyunit onto your path for convenience (e.g. `/usr/local/bin`).

## Usage

Basic usage:

```text
asyunit <test_fixture>
```

Given a test file `test.asy` with content:

```asymptote
void test_me() {
  assert(2+2 == 4);
}
```

Then the test runner can be invoked like so:

```text
> asyunit test.asy
ok test.asy test_me
```

If tests are graphical (shipout should be called) then specify an artifact dir
for with the `-g` flag:

```text
> asyunit -g build test.asy
ok test.asy test_me
```

Pictures will be shipped out to the build dir and intermediate files kept (`asy
-keep` is used). `asyunit` will then compare the intermediate files against
expectations. It will look for those expectations next to your tests, e.g.
`test_test_me_.log`, `test_test_me_.eps`. `.eps` files will be compared with
ImageMagick: a diff score is given (metric AE) and a diff image produced for
visual comparison.

You can also filter which tests to run with a pattern:

```text
asyunit <test_fixture> -k pattern
```

Where `pattern` will be prefixed with `test_` and matched against the function
names. The pattern should follow `sed`'s pattern rules. For example:

```text
> asyunit -k 'circle.*' test.asy
ok test.asy test_circle_big
ok test.asy test_circle_small
```

To print the version: `asyunit -v`:

```text
> asyunit -v
asyunit 1.0
```

To get help on usage: `asyunit -h`.

Note the output tries to be
[TAP](http://testanything.org/tap-specification.html) compliant but we have a
little way to get there (see To Do below)

## Writing Tests

asyunit will call all functions in the test file given that are prefixed with `test_`:

```asymptote
void test_success() {
  // I'm a test case
}

void something_else() {
  // I am not
}
```

Any other code in the file at the top scope will also be evaluated once before
the test functions are run.

## To Do

Regarding asyunit, our known shortcomings in order of priority:

* Sometimes tests fail writing to a .log file, my guess is a single
  asy run with multiple files is somehow stepping on itself or not giving up
  it's own lock in time. Might be hard to reproduce. Might only be graphical
  tests? Maybe it's when ninja runs two asy's at same time? It happens a lot
  with figular.
* We do not detect certain failures in tests, e.g.

```bash
# figular/selector.asy: 84.29: accessing private field outside of structure
```

  ...is marked ok.

* A seg faults within the asymptote test code results in no output and a 0
  return code, therefore false positive. This can be reproduced with (for
  example) any test that has infinite recursion.

* In `.tex`, `.log` files the build path is hard-coded which means the diffs
  will fail if the build path is changed. In `.tex` files it appears near the
  start:

```tex
\def\ASYprefix{BUILD_PATH}
```

  In `.log` files it will list the built files several times by full path.

* Allow to discover tests in a dir/tree so our build files are simpler. At the
  moment we are liable to miss a dependency and thus not run tests when we
  should. This will mean slower builds as we run more/all tests on each run and
  cannot use ninja's parallism. So also consider adding failed-first option like
  [pytest](https://docs.pytest.org/en/7.1.x/how-to/cache.html)
  Or speeding up tests with concurrency below. You could also add 'last-failed'
  option but this would only be safe in an IDE, main builds should always run
  all tests as we can't know what files may have changed. Or also gives builds a
  way to invalidate the last-failed cache when a file changes.
  * We'll need a new way to specify which tests are graphical.
* Run tests concurrently. At the moment we use ninja to do this for us but at
  the cost of complicated build files. So the only gain in the short-term is
  cleaner build files.
  * Graphical tests may collide in the build dir as asy appears to first output
    to a hard-coded filename(s) before renaming to that which was specified in
    shipout/cmdline. We need to know the 'root' of the tests in order to split
    up the build output sensibly and that means changing the way we discover
    tests (above) first.
  * We may be able to use GNU parallel to automatically chunk up a file of test
    fixtures (one per line as now) to apply to a suitable number of parallel asy
    processes it manages.
* Clean up old test files on each run
* Use of lualatex engine is hardcoded
* It would be useful to have tests that expect asy code to `abort`, as there is
  no exception handling in asy.
* Better graphical test example above in docs
* Be sure/explicit that asymptote has disabled external process calls
* Investigate hardening techniques relevant to asyunit/BASH
* Measure test coverage in order to meet more CII badge requirements, e.g.
  [kcov](https://github.com/SimonKagstrom/kcov),
  [bcov](https://github.com/abenkhadra/bcov),
  [bashcov](https://github.com/infertux/bashcov),
  [lcov](https://github.com/linux-test-project/lcov),
  [lcov.sh](https://github.com/javanile/lcov.sh)

General:

* Create installation packages and/or instructions
* Create setup script/container for developers to contribute
* Implement an Asymptote linter
* Implement a language server for Asymptote to enable auto-completion etc in
  editors/IDEs.

Old concerns, fragments of thoughts:

* Add fuzzing
* .tex and .log expectation files end up with hard-coded paths in them with
  usernames.
* Writing files with output() they end up in the build dir if we specify no
  path. Reading with input however we have to specify the build path. WTF
* We shipout currentpicture if not empty to help the user however there might be
  a bug that means if you ship anything else out currentpicture will get shipped
  anyway and you get extra output you don't want:
  [Asymptote / Discussion / Help: shipout(otherpicture) affects currentpicture](https://sourceforge.net/p/asymptote/discussion/409349/thread/bcd61b2ba5/)
  Note however in other of my tests I have not seen this double shipout, so
  there is something that only triggers the bug in certain situations.
* Closely related the tests for drawcard produced extra files, a .tex, a
  `_0.eps` etc. Yet other more complicated tests do not. What triggers this? Its
  separating text from drawing it seems but how come it doesn't do this all the
  time?
  * Following on from this asyunit insists we have an expectionat for the .eps
    but these cases the .eps does not contain any of the labels so is not a
    complete expectation for the test. Only the PDF seems to have everything?
* The graphical diff concentrates on what's changed based on original but we
  don't get to see what's new, specially a pain if new picture is bigger as
  elements are just missing
* If an old graphical result is left in the build dir it can cause test fails as
  an expectation is still required for it?
* I don't think the diff on the eps files is useful everyday, and it scrolls all
  results up the terminal. Make it optional.
  * Part way done, I have made it quiet by default but no option yet.
* If the final expectation is missing I have seen it report expectations missing
  for both last two tests?
* Not clear how to use graphical output, we just call shipout? But I get eps and
  pdf.
* Let's follow pytest defaults for
  * How to test several files
  * Where to 'build' to - perhaps default is cur dir with an option to override?
* Some assert routines with default text would be handy
* asyunit gives up after any fail which is confusing if you want to refresh
  graphical results regardless of earlier fails
* Need to specify a dir so we can run all tests
* assert is an asymptote construct to be clear.
* What if don't call assert? How do we fail a test? Is it exit code basically?
* Bug - if we don't have a build dir created or tests that need one, the run
  will output an error:
  `find: ‘build’: No such file or directory`
  However this does not fail the run: rc=0. Should we just create the output
  dir?
* When a test fails we do not see the name of the one that failed,
  confusing.

## Contributing

Please see our guide at [CONTRIBUTING.md](CONTRIBUTING.md)

## License

We declare our licensing by following the [REUSE
specification](https://reuse.software/) - copies of applicable licenses are
stored in the `LICENSES` directory. Here is a summary:

* All source code is licensed under [GPL-3.0-or-later](https://spdx.org/licenses/GPL-3.0-or-later.html).
* If it's not executable, including the text when extracted from code, it is
  licensed under [CC-BY-SA-4.0](https://spdx.org/licenses/CC-BY-SA-4.0.html).

For more accurate information, check individual files.

asyunit is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.
